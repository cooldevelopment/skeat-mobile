import React, { Component } from 'react';
import { Provider } from 'mobx-react';
import stores from './app/stores';
import PushNotification from 'react-native-push-notification';
import { StyleProvider, Root } from 'native-base';
import getTheme from './native-base-theme/components';
import custom from './native-base-theme/variables/custom';
import { Router, Scene } from 'react-native-router-flux';

/* Auth */
import LoginScreen from './app/modules/auth/login.screen';
import ActivationCodeScreen from './app/modules/auth/activation_code.screen';
import OnBoardingScreen from './app/modules/auth/on_boarding.screen';

/* User */
import AddressScreen from './app/modules/user/address.screen';
import CreditCardScreen from './app/modules/user/credit_card.screen';

/* Splash */
import SplashScreen from './app/modules/splash.screen';

/* Home */
import FeedScreen from './app/modules/feed/feed.screen';

/* Menu */
import CategoriesScreen from './app/modules/menu/categories.screen';
import ProductsScreen from './app/modules/menu/products.screen';
import AdditionalsScreen from './app/modules/menu/additionals.screen';

/* Order */
import OrderScreen from './app/modules/order/order.screen';

export default class App extends Component {

  componentDidMount() {
    PushNotification.configure({
      autoCancel: true,
      /**
       *  (optional) Called when Token is generated (iOS and Android) 
       */
      onRegister: (token) => {
        console.log('TOKEN:', token);
      },
      /**
       * (required) Called when a remote or local notification is opened or received
       */
      onNotification: (notification) => {
        console.log('NOTIFICATION:', notification);
      },
      /**
       * ANDROID ONLY: GCM Sender ID (optional — not required for local notifications, 
       * but is need to receive remote push notifications)
       */
      senderID: "160490025896",
      /**
       * IOS ONLY (optional): default: all — Permissions to register.
       */
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },
      /**
       * Should the initial notification be popped automatically
       * default: true
       */
      popInitialNotification: false,
      /**
      * (optional) default: true
      * Specified if permissions (ios) and token (android and ios) will requested or not,
      * if not, you must call PushNotificationsHandler.requestPermissions() later
      */
      requestPermissions: true,
    });
  }
  
  /**
   * Component renderer.
   * 
   */
	render() {
		return (
      <Provider stores={stores} style={{ backgroundColor: '#f8e71c' }}>
				<StyleProvider style={getTheme(custom)}>
          <Root>
						<Router>
							<Scene key="root">
                <Scene 
                  key="splash"
                  component={SplashScreen}
                  initial={true}
                  headerMode="none"
                  hideNavBar={true}
								/>
                <Scene 
                  key="login"
                  component={LoginScreen}
                  headerMode="none"
                  renderBackButton={() => (null)}
                  left={() => null}
                  hideNavBar={true}
                  title="Login"
								/>
                <Scene 
                  key="activationCode"
                  component={ActivationCodeScreen}
								/>
                <Scene 
                  key="feed"
                  component={FeedScreen}
                  renderBackButton={() => (null)}
                  left={() => null}
								/>
                <Scene 
                  key="categories"
                  component={CategoriesScreen}
								/>
                <Scene 
                  key="products"
                  component={ProductsScreen}
								/>
                <Scene 
                  key="additionals"
                  component={AdditionalsScreen}
								/>
                <Scene 
                  key="onBoarding"
                  component={OnBoardingScreen}
                  renderBackButton={() => (null)}
                  left={() => null}
								/>
                <Scene 
                  key="address"
                  component={AddressScreen}
                  renderBackButton={() => (null)}
                  left={() => null}
								/>
                <Scene 
                  key="creditCard"
                  component={CreditCardScreen}
                  renderBackButton={() => (null)}
                  left={() => null}
								/>
                <Scene 
                  key="order"
                  component={OrderScreen}
								/>
							</Scene>
						</Router>
					</Root>
				</StyleProvider>
			</Provider>
		);
	}
}