import React, { Component } from 'react';
import { observer } from 'mobx-react/native';
import { Platform } from 'react-native';
import {
	Content,
	View,
	List, 
	ListItem,
	Text,
	Left,
	Right,
	Icon,
	Item,
	Input,
	Button
} from 'native-base';
import ModalSelector from 'react-native-modal-selector';
import { Actions } from 'react-native-router-flux';

@observer
export default class Feed extends Component {
	/**
	 * Component constructor.
	 * 
	 * @param {Object} props
	 * 
	 */
	constructor(props) {
		super(props);
		this.state = {
			selected: "key0"
		};

		this.onValueChange = this.onValueChange.bind(this);
		this.handleOnPress = this.handleOnPress.bind(this);
	}

	componentWillMount() {
	}
	
	/**
	 * Get the current location and get the companies that are near by
	 * 
	 */
	componentDidMount() {
		const { locale, company, cart, auth } = this.props.stores;
		/**
		 * If there is a unfinished order redirect to orders screen.
		 */
		if (auth.user !== null) {
			cart.getLastOrder(auth.user.id);
		}
		
		navigator.geolocation.getCurrentPosition(
			position => {
				locale.getLocaleByLatLng({
					lat: position.coords.latitude,
					lng: position.coords.longitude
				})
					.then(() => {
						if (locale.locale !== null) {
							company.getCompaniesByLocale(locale.locale.id);
						}
					});
			},
			error => { 
				this.setState({ 
					error: error.message 
				});
			},
			{ 
				enableHighAccuracy: true, 
				timeout: 20000, 
				maximumAge: 1000 
			}
		);
	}

	/**
	 * Change value of the selected locale.
	 * 
	 * @param {String} value 
	 * 
	 */
	onValueChange(value) {
		this.setState({
			selected: value
		});
	}

	/**
	 * Handle the press event on the company and 
	 * set the id for the current cart.
	 * 
	 */
	handleOnPress(id, e) {
		const { stores } = this.props;

		Actions.categories({ company_id: id })
		stores.cart.cartCompany(id);
	}

	/**
	 * Component renderer.
	 * 
	 */
	render() {
		const { locale, company, cart } = this.props.stores;

		return (
			<Content style={{ padding: 20 }}>
				{cart.order && cart.order.status &&
				<View
					style={{
						flex: 1,
						flexDirection: 'column',
						justifyContent: 'center',
						alignItems: 'center',
						zIndex: 3,
					}}>
					<Text>Você tem um pedido sendo processado.</Text>
					<Button
						block
						dark
						bordered
						rounded
						style={{
							marginTop: 15,
							marginBottom: 10,
							backgroundColor: "#ffd740",
							borderColor: "#ffd740",
							paddingTop: Platform.OS === 'android' ? 20 : 0
						}}
						onPress={() => Actions.order()}>
						<Text>VER PEDIDO</Text>
					</Button>
				</View>}
				<ModalSelector
					cancelText="Fechar"
					data={locale.locales.map(locale => locale)}
					onChange={option => {
						if(option !== null) {
							locale.setLocale(option.id);
						}
						if (locale.locale !== null) {
							company.getCompaniesByLocale(locale.locale.id);
						}
					}}>
					<Item 
						style={{ 
								marginBottom: 10, 
								borderColor: "#ffd740"
						}} 
						rounded>
						<Icon style={{color: "#ffd740"}} name='map' />
						<Input
							placeholder='Aonde você esta?'
							value={
								(locale.locale) 
									? locale.locale.name 
									: "Buscando..."
							} />
					</Item>
				</ModalSelector>
				<List style={{ flex: 1 }}>
					{company.companies.map((c, i) => {
						return (
							<ListItem 
								key={i} 
								onPress={() => this.handleOnPress(c.id)}>
								<Left>
									<Text>{c.info.fantasy_name}</Text>
								</Left>
								<Right>
									<Icon name="arrow-forward"/>
								</Right>
							</ListItem>
						);
					})}
				</List>
			</Content>
		);
	}
}

