import React, { Component } from 'react';
import { Platform } from 'react-native';
import {
	Button,
	Item,
	Icon,
	Text,
	Form,
	Toast
} from 'native-base';
import { TextInputMask } from 'react-native-masked-text'
import { LoginManager, AccessToken, GraphRequestManager, GraphRequest } from 'react-native-fbsdk';
import { Actions } from 'react-native-router-flux';

export default class Login extends Component {

	constructor(props) {
		super(props);

		this.sendActivationCode = this.sendActivationCode.bind(this);
		this.facebookLogin = this.facebookLogin.bind(this);
		this.responseInfoCallback = this.responseInfoCallback.bind(this);
		this.skipLogin = this.skipLogin.bind(this);
	}

	facebookLogin() {
		LoginManager.logInWithReadPermissions(['public_profile'])
			.then((result) => {
			if(result.isCancelled) {
				console.log('Login cancelled');
			} else {
				AccessToken.getCurrentAccessToken().then(
					(data) => {
						let accessToken = data.accessToken;

						const infoRequest = new GraphRequest(
							'/me',
							{
								accessToken: accessToken,
								parameters: {
									fields: {
										string: 'email, name, first_name, middle_name, last_name'
									}
								}
							},
							this.responseInfoCallback
						);
						// Start the graph request.
						new GraphRequestManager().addRequest(infoRequest).start();
					});
			}
		}).then((err) => {
			console.log(err);
		})
	}

	responseInfoCallback = (error, result) => {
		const { auth } = this.props.stores;
		if (error) {
			alert('Error fetching data: ' + error.toString());
		} else {
			auth.socialLogin({
				id: result.id,
				fullname: result.name,
				email: result.email,
			}).then(() => {
				if(auth.user.creditCards.length > 0) {
					Actions.feed();
				} else {
					Actions.onBoarding();
				}
			});
		}
	};

	sendActivationCode() {
		const { auth } = this.props.stores;
		const phone = auth.phone;

		if(phone.length >= 14) {
			auth.sendActivationCode(phone).then((res) => {
				Actions.activationCode();
			});
		} else {
			Toast.show({
				text: "Telefone inválido!",
				buttonText: "OK",
			});
		}
	}

	skipLogin() {
		Actions.feed();
	}

	render() {
		const { auth } = this.props.stores;

		return (
			<Form>
				<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
					<Icon style={{color: "#ffd740"}} name='phone' />
					<TextInputMask
						style={{
							flex: 1,
							fontSize: 17,
							paddingRight: 5,
							height: 50,
							color: "#000000"
						}}
						placeholder="Telefone"
						type={'cel-phone'}
						value={auth.phone}
						onChangeText={(phone) => auth.handleChange('phone', phone)}/>
				</Item>
				<Button 
					block
					dark
					bordered
					rounded
					style={{ 
						marginBottom: 10, 
						backgroundColor: "#ffd740", 
						borderColor: "#ffd740",
						paddingTop: Platform.OS === 'android' ? 15 : 0 
					}}
					onPress={this.sendActivationCode}>
					<Text>ENVIAR</Text>
				</Button>
				<Text 
					style={{ 
						justifyContent: 'center', 
						alignItems: 'center', 
						alignSelf: 'center', 
						paddingBottom: 10,
						fontSize: 12 
					}}>ou</Text>
				<Button 
					block
					rounded
					style={{ 
						marginBottom: 10, 
						paddingTop: Platform.OS === 'android' ? 15 : 0 
					}}
					onPress={this.facebookLogin}>
					<Text>LOGAR COM FACEBOOK</Text>
				</Button>
				<Button 
					block
					transparent 
					dark
					style={{
						marginBottom: 10,
						paddingTop: Platform.OS === 'android' ? 15 : 0 
					}}
					onPress={this.skipLogin}>
					<Text>Pular Login</Text>
				</Button>
			</Form>
		);
	}

}
