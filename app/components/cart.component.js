import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import {
	StyleSheet,
	View,
	Platform
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {
	Card,
	CardItem,
	Text,
	List,
	ListItem,
	Left,
	Right,
	Input,
	Item,
	Icon as NIcon,
	H3,
	Button,
	Body,
	SwipeRow,
	Toast
} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'
import Drawer from 'react-native-bottom-drawer';

@inject("stores")
@observer
export default class OrderScreen extends Component {
	/**
	 * Component Constructor.
	 * 
	 */
	constructor(props) {
		super(props);
		this.state = {
			coupon: ""
		}
		this.handleCheckout = this.handleCheckout.bind(this);
		this.handleOnChange = this.handleOnChange.bind(this);
		this.handleCouponSubmit = this.handleCouponSubmit.bind(this);
	}

	/**
	 * On input change get search for the coupon code in the 
	 * companies.coupons array and update total price if it exists.
	 * 
	 */
	handleOnChange(value) {
		this.setState({
			coupon: value
		})
	}
	
	/**
	 * Get the company for this cart, then get the company coupons.
	 * Check if the value inputed matches one of the coupons.
	 * Finaly update the final amount.
	 * 
	 */
	handleCouponSubmit() {
		const { coupon } = this.state;
		const { company, cart } = this.props.stores;
		
		company.companies
			.filter(c => c.id === cart.company_id)
			.map(cp => {
				return cp.coupons
					.filter(cpp => {
						cart.updateDiscount();
						if (cpp.code === coupon) {
							cart.updateDiscount(cpp.discount_percentage);
						}
					})
			});
	}

	/**
	 * Verify if skeat user is logged or else redirect to login page.
	 * If the logged user has no credit cards, redirect to the on boarding page
	 * fot the complete profile form.
	 * Else send the order to the company panel and redirect to the order status page.
	 * 
	 */
	async handleCheckout() {
		const { auth, cart } = this.props.stores;
		
		/** 
		 * Check user auth situation
		 */
		if (!auth.isLogged) {
			/**
			 * it`s necessary a logged user to make a order.
			 */
			Actions.login();
		} else if (auth.user.creditCards 
			&& auth.user.creditCards.length === 0) {
			/**
			 * Redirect to onboarding if the user has not filled 
			 * credit card info.
			 */
			Actions.onBoarding();
		} else {
			/**
			 *  Handle user order and send the data to the company..
			 */
			if (await cart.saveOrder(auth.user.id)) {
				/**
				 *  Then redirect to order status.
				 */
				Actions.order();
			} else {
				Toast.show({
					text: "Houve um erro de conexão com o resteurante, tente novamente.",
					buttonText: "OK",
				});
			}
		}
	}

	/**
	 * Navigation current title.
	 * 
	 */
	static navigationOptions = ({ navigation }) => ({
		title: 'Produtos',
	});

	/**
	 * Component Renderer.
	 * 
	 */
	render() {
		const { coupon } = this.state;
		const { stores } = this.props;
		
		if (stores.cart.items && stores.cart.items.length > 0) {
			return (
				<Drawer 
					header="Carrinho"
					headerHeight={0} 
					teaserHeight={Platform.OS === 'android' ? 130 : 110} 
					containerBackgroundColor={"#fff"}>
					<View style={styles.cartHeader}>
						<Left>
							<Icon 
							style={styles.cartIcon} 
							name="ios-cart-outline" />
						</Left>
						<Right>
							<H3 style={styles.cartTitle}>
								{`Total: R$ ${stores.cart.getAmount().toFixed(2)}`}
							</H3>
						</Right>
					</View>
					<View style={styles.cartWrapper}>
					<List style={{ flex: 1, marginRight: 20 }}>
						{stores.cart.items.map((i, index) => {
							return (
									<ListItem key={index}>
										<Left>
											<Text style={styles.cartProductTitle}>
												{i.name}
											</Text>
										</Left>
										<Right>
											<View
												style={{
													flex: 1,
													flexDirection: 'row'
												}}>
												{/* {i.additionals && i.additionals.length > 0 && */}
													<Button
														rounded
														light
														onPress={() => {
															stores.cart.customizeItem(i, index);
															Actions.additionals({ product_id: i.id });
														}}
														style={styles.cartActions}>
														<MaterialIcon
															name="edit" 
															style={{
																fontSize: 20,
																width: Platform.OS === 'android' ? 30 : 20,
																height: Platform.OS === 'android' ? 30 : 20,
																color: '#000',
															}}/>
													</Button>
												{/* } */}
												<Button
													rounded
													light
													onPress={() => {
														stores.cart.deleteItem(index);
													}}
													style={styles.cartActions}>
													<MaterialIcon
														active
														name="delete"
														style={{
															fontSize: 20,
															width: Platform.OS === 'android' ? 30 : 20,
															height: Platform.OS === 'android' ? 30 : 20,
															color: '#000',
														}} />
												</Button>
											</View>
										</Right>
									</ListItem>
							);
						})}
						</List>
						<View style={[
							styles.cartContainer, 
							{ flexDirection: 'row' }
						]}>
							<Item 
								style={{ 
									marginTop: 15, 
									borderColor: "#ffd740",
									flex: 5 
								}} 
								rounded>
								<NIcon 
									style={{ 
										color: "#ffd740", 
										paddingTop: 6
									}} 
									name='card-membership' />
								<Input
									value={coupon}
									placeholder='Digite seu código de Cupom'
									onChangeText={this.handleOnChange} />
							</Item>
								<Button
									rounded
									style={{ 
										backgroundColor: "#fff159", 
										marginTop: 20, 
										marginLeft: 10, 
										flex: 1,
										justifyContent: 'center',
										alignItems: 'center'
									}}
									onPress={this.handleCouponSubmit}>
									<Text
										style={{
											color: "#000",
											paddingTop: Platform.OS === 'android' ? 15 : 0
										}}>OK</Text>
								</Button>
						</View>
						<View style={[styles.cartContainer, { marginTop: 15 }]}>
							<Card>
								<CardItem>
									<Left>
										<Text style={styles.cartTotalText}>
											Total
										</Text>
									</Left>
									<Right>
										<Text style={styles.cartTotalPrice}>
											R$ {stores.cart.getAmount().toFixed(2)}
										</Text>
									</Right>
								</CardItem>
							</Card>
							{stores.auth.user.creditCards && 
								stores.auth.user.creditCards.length > 0 && 
								<Card>
									{stores.auth.user.creditCards.map((cc, i) => {
										return (
											<CardItem key={i}>
												<Left>
													<Text style={styles.cartTotalText}>
														<MaterialIcon style={{
															color: "#ffd740", 
															fontSize: 30, 
															marginRight: 10 
															}} name='credit-card'/>
													</Text>
													<Text>{cc.brand}</Text>
												</Left>
												<Right>
													<Text style={styles.cartTotalPrice}>
														&#8226;&#8226;&#8226;&#8226; {cc.last4}
													</Text>
												</Right>
											</CardItem>
										)
									})}
								</Card>
							}
						</View>
						<View style={styles.cartContainer}>
							<Button 
								rounded 
								block 
								style={{ 
									backgroundColor: "#fff159", 
									marginTop: 20 
								}} 
								onPress={this.handleCheckout}>
								<Text 
									style={{
										color: "#000",
										paddingTop: Platform.OS === 'android' ? 15 : 0 
									}}>
										Finalizar Compra
									</Text>
							</Button>
						</View>
					</View>
				</Drawer>
			);
		}
		return null;
	}
}

/**
 * Component Styles.
 * 
 */
const styles = StyleSheet.create({
	cartWrapper: { 
		flex: 1, 
		paddingTop: 20,
		paddingBottom: 100,
	},
	cartProductTitle: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
	},
	cartProductDescription: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
	},
	cartProductEdit: {
		fontSize: 24,
		color: 'white',
	},
	cartProductDelete: {
		fontSize: 24,
		color: 'white',
	},
	cartProductButton: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
	},
	nestedButtonView: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	cartTotalText: {
		fontSize: 22
	},
	cartTotalPrice: {
		flex: 1,
		display: 'flex',
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
		fontSize: 22
	},
	cartHeader: {
		height: 50,
		backgroundColor: '#fff159',
		paddingLeft: 8,
		paddingRight: 20,
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-start',
		zIndex: 100
	},
	cartTitle: {
		color: "#000"
	},
	cartIcon: {
		color: "#000",
		fontSize: 30
	},
	cartContainer: {
		paddingLeft: 10, 
		paddingRight: 10
	},
	cartActions: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 40,
		height: 40,
		paddingLeft: Platform.OS === 'android' ? 10 : 0,
		paddingTop: Platform.OS === 'android' ? 15 : 5,
		marginLeft: 10,
	}
});