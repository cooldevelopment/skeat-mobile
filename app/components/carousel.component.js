/**
 * Created by matheusmorett on 20/07/18.
 */
import React, { Component } from 'react';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import { 
    View, 
    Text, 
    StyleSheet, 
    Dimensions, 
    Image, 
    TouchableWithoutFeedback 
} from 'react-native';
import {Actions} from 'react-native-router-flux';

const { width, height } = Dimensions.get('window');
export default class CarouselComponent extends Component {

    _renderItem({item, index}, parallaxProps) {
        return (
            <TouchableWithoutFeedback onPress={() => Actions.products({ category_id: item.id })} key={item.id}>
                <View style={styles.itemcarousel}>
                    <Image source={{uri: item.photo}} style={styles.image} />
                    <View style={styles.innerFrame}>
                        <Text style={styles.title}>{item.name}</Text>
                        <Text style={styles.description}>{item.description}</Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }

    render() {
        const { type, data } = this.props;
        const finalData = data.map((d) => {
            d.type = type;
            return d;
        })
        return (
            <Carousel
                layout={'default'}
                data={finalData}
                renderItem={this._renderItem.bind(this)}
                sliderWidth={width}
                itemWidth={width}
                firstItem={0}
            />
        );
    }
}

const heightScale = Math.round(height / 4.3);

const styles = StyleSheet.create({
    itemcarousel: {
        // flex: 1,
        height: heightScale,
        flexDirection: 'row',
        paddingRight: 10,
        paddingLeft: 10
    },
    image: {
        width: 150,
        height: heightScale,
        backgroundColor: '#ccc'
    },
    innerFrame: {
        flex: 1,
        width: width,
        height: heightScale,
        justifyContent: 'center',
        borderColor: '#ccc',
        borderStyle: 'solid',
        borderWidth: 1,
        paddingLeft: 10,
        paddingRight: 10,
    },
    title: {
        color: '#000',
        opacity: 1,
        fontSize: 26,
        textAlign: 'left',
        fontWeight: '600',
        marginVertical: 5,
        backgroundColor: 'transparent'
    },
    description: {
        color: '#000',
        opacity: 1,
        textAlign: 'left',
        fontWeight: '600',
        backgroundColor: 'transparent'
    }
});

