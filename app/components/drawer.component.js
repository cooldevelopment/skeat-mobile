/**
 * Created by matheusmorett on 10/06/18.
 */
import React, { Component } from 'react';
import { Drawer, Text } from 'native-base';
export default class Drawer extends Component {
    closeDrawer = () => {
        this.drawer._root.close()
    };
    openDrawer = () => {
        this.drawer._root.open()
    };
    render() {
        return (
            <Drawer
                ref={(ref) => { this.drawer = ref; }}
                content={<Text> Drawer </Text>}
                onClose={() => this.closeDrawer()} >
            </Drawer>
        );
    }
}
