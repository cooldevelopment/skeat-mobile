import React, { Component } from 'react';
import { inject } from 'mobx-react';
import {
	Container,
	Content
} from 'native-base';
import {
	StyleSheet,
} from 'react-native';
import Feed from './../../components/feed.component';

@inject("stores")
export default class FeedScreen extends Component {
  /**
   * Component constructor.
   * 
   * @param {Object}} props 
   * 
   */
	constructor(props) {
		super(props);
	}

  /**
   * Nav params/
   * 
   */
	static navigationOptions = ({navigation}) => ({
		title: 'Skeat',
	});

  /**
   * Component renderer.
   * 
   */
	render() {
		return (
			<Container>
				<Content scrollEnabled={true} style={styles.container}>
					<Feed {...this.props} />
				</Content>
			</Container>
		);
	}
}

/**
 * Componenet styles.
 * 
 */
const styles = StyleSheet.create({
	container: {
		backgroundColor: "#fff"
	},
});
