import React, { Component } from 'react';
import {
	Container,
	Content,
	Button,
	Form,
	Item,
	Input,
	Text,
	H2,
	Toast,
	Icon
} from 'native-base';
import {
	View,
	StyleSheet,
	Platform
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text'
import { Actions } from 'react-native-router-flux';

import { inject, observer } from 'mobx-react';

@inject("stores")
@observer
export default class AddressScreen extends Component {
  /**
   * Componenet constructor.
   * 
   * @param {*} props
   *  
   */
	constructor(props) {
		super(props);
		this.handleSaveUser = this.handleSaveUser.bind(this);
	}

  /**
	 * Veriofy if all required fields ahve been filled 
	 * and save in to the user.
	 * 
	 */
	handleSaveUser() {
    const { auth } = this.props.stores;
    
    if (auth.creditCard.fullname.length > 0 &&
      auth.creditCard.number.length > 0 &&
      auth.creditCard.cvc.length > 0 &&
      auth.creditCard.expirationMonth.length > 0 &&
      auth.creditCard.expirationYear.length > 0) {
      auth.saveUserCreditCard().then((res) => {
				if (res.err === undefined) {
					Actions.feed();
        } else {
          Toast.show({
            text: `Ocorreu um erro. ${res.err.error.errors[0].description}`,
            buttonText: "OK",
          });
        }
      })
    } else {
      Toast.show({
        text: "Preencha todos os campos marcados como obrigatórios (*).",
        buttonText: "OK",
      });
    }
	}

  /**
   * Componenet renderer.
   * 
   */
	render() {
		const { auth } = this.props.stores;

		return (
			<Container style={{backgroundColor: "#ffffff"}}>
				<Content scrollEnabled={false} style={styles.container}>
					<View style={styles.header}>
						<H2>
							Complete seu Perfil
						</H2>
					</View>
					<View style={styles.loginForeground}>
						<Form>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='account-circle' />
								<Input
									value={auth.creditCard.street}
									placeholder='* Nome do títular'
									onChangeText={(fullname) => auth.handleChangeUserCreditCard('fullname', fullname)}
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='credit-card' />
								<TextInputMask
									style={{
										flex: 1,
										fontSize: 17,
										paddingRight: 5,
										height: 50,
										color: "#000000"
									}}
									// here we set the custom component and their props.
									customTextInputProps={{
										label:'Número do Cartão'
									}}

									type={'credit-card'}
									options={{
										format: '9999 9999 9999 9999'
									}}
									value={auth.creditCard.number}
									onChangeText={(number) => auth.handleChangeUserCreditCard('number', number)}
									placeholder="* Número do Cartão"
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='edit' />
								<Input
									keyboardType='numeric'
									maxLength={4}
									value={auth.creditCard.cvc}
									placeholder='* CVC'
									onChangeText={(cvc) => auth.handleChangeUserCreditCard('cvc', cvc)}
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='date-range' />
								<Input
									value={auth.user.expirationMonth}
									placeholder='* MM'
									onChangeText={(expirationMonth) => auth.handleChangeUserCreditCard('expirationMonth', expirationMonth)}
									keyboardType='numeric'
									maxLength={2}
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='date-range' />
								<Input
									value={auth.user.expirationYear}
									placeholder='* AA'
									onChangeText={(expirationYear) => auth.handleChangeUserCreditCard('expirationYear', expirationYear)}
									keyboardType='numeric'
									maxLength={2} />
							</Item>
							<Button 
								block
								rounded
								style={{
									marginBottom: 10,
									paddingTop: Platform.OS === 'android' ? 15 : 0 
								}}
								onPress={this.handleSaveUser}>
								<Text>FINALIZAR</Text>
							</Button>
						</Form>
					</View>
				</Content>
			</Container>
		);
	}
}

/**
 * Componenet renderer.
 * 
 */
const styles = StyleSheet.create({
	container: {
		backgroundColor: '#ffffff',
	},
	header: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		paddingBottom: 20,
		paddingTop: 20,
		flex: 1,
	},
	radioText: {
		width: 100,
		flex: 1
	},
	loginForeground: {
		flex: 1,
		paddingTop: 20,
		paddingLeft: 10,
		paddingRight: 10,
		paddingBottom: 90,
		bottom: 0
	},
	list: {
		paddingBottom : 20
	}
});



