import React, { Component } from 'react';
import {
	Container,
	Content,
	Button,
	Form,
	Item,
	Input,
	Text,
	H2,
	Toast,
	Icon
} from 'native-base';
import {
	View,
	StyleSheet,
	Platform
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text'
import { Actions } from 'react-native-router-flux';

import { inject, observer } from 'mobx-react';

@inject("stores")
@observer
export default class AddressScreen extends Component {
  /**
   * Componenet constructor.
   * 
   * @param {*} props 
   * 
   */
	constructor(props) {
		super(props);
		this.handleSaveUser = this.handleSaveUser.bind(this);
	}

  /**
	 * Veriofy if all required fields ahve been filled 
	 * and save in to the user.
	 * 
	 */
	handleSaveUser() {
		const { auth } = this.props.stores;

		auth.saveUserAsCustomer().then((res) => {
			if (!res.error) {
				Actions.creditCard();
			} else {
				Toast.show({
					text: `Ocorreu um erro ao salvar o endereço!:${res.error.error.errors[0].description}`,
					buttonText: "OK",
				});
			}
		})
	}

  /**
   * Component renderer.
   * 
   */
	render() {
		const { auth } = this.props.stores;

		return (
			<Container style={{backgroundColor: "#ffffff"}}>
				<Content scrollEnabled={false} style={styles.container}>
					<View style={styles.header}>
						<H2>
							Complete seu Perfil
						</H2>
					</View>
					<View style={styles.loginForeground}>
						<Form>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='edit' />
								<TextInputMask
									style={{
										flex: 1,
										fontSize: 17,
										paddingRight: 5,
										height: 50,
										color: "#000000"
									}}
									// here we set the custom component and their props.
									customTextInputProps={{
										label:'CEP'
									}}
									type={'zip-code'}
									options={{
										format: '99999-999'
									}}
									placeholder="CEP"
									value={auth.user.cep}
									onChangeText={(cep) => auth.handleChangeUser('cep', cep)}
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='edit' />
								<Input
									value={auth.user.street}
									placeholder='Endereço'
									onChangeText={(street) => auth.handleChangeUser('street', street)}
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='edit' />
								<Input
									value={auth.user.street_number}
									placeholder='Número'
									onChangeText={(street_number) => auth.handleChangeUser('street_number', street_number)}
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='edit' />
								<Input
									value={auth.user.district}
									placeholder='Bairro'
									onChangeText={(district) => auth.handleChangeUser('district', district)}
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='edit' />
								<Input
									value={auth.user.city}
									onChangeText={(city) => auth.handleChangeUser('city', city)}
									placeholder='Cidade'
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='edit' />
								<Input
									value={auth.user.state}
									onChangeText={(state) => auth.handleChangeUser('state', state)}
									placeholder='Estado'
								/>
							</Item>
							<Button 
								block
								rounded
								style={{
									marginBottom: 10,
									paddingTop: Platform.OS === 'android' ? 15 : 0 
								}}
								onPress={this.handleSaveUser}>
								<Text>PRÓXIMO</Text>
							</Button>
						</Form>
					</View>
				</Content>
			</Container>
		);
	}
}

/**
 * Componenet styles.
 * 
 */
const styles = StyleSheet.create({
	container: {
		backgroundColor: '#ffffff',
	},
	header: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		paddingBottom: 20,
		paddingTop: 20,
		flex: 1,
	},
	radioText: {
		width: 100,
		flex: 1
	},
	loginForeground: {
		flex: 1,
		paddingTop: 20,
		paddingLeft: 10,
		paddingRight: 10,
		paddingBottom: 90,
		bottom: 0
	},
	list: {
		paddingBottom : 20
	}
});