import React, { Component } from 'react';
import { Easing, Animated, View } from 'react-native';
import { inject } from 'mobx-react';
import { Actions } from 'react-native-router-flux';

@inject("stores")
export default class SplashScreen extends Component {
  /**
   * Initial states.
   * 
   */
  state = {
    fadeAnim: new Animated.Value(0),  // Initial value for opacity: 0
  }

  /**
   * Set the splashscreen time. and redirects to the 
   * needed page.
   * 
   */
	componentWillMount() {
		const { stores } = this.props;
    const { auth } = stores;

    /**
     * Start animation.
     */
    Animated.timing(                  
      this.state.fadeAnim,            
      {
        toValue: 1, 
        easing: Easing.back(),
        duration: 1000,              
      }
    ).start();
    
    /**
     * Set the splash time out and redirect user depending on 
     * the following situations.
     * 
     * Feed - If he has credit card registered.
     * On Boarding - If he hasn`t filling all the informations
     * Login - If there isn`t a user in the session (local storage)
     */
    setTimeout(() => {
      if(auth.isLogged) {
        if (auth.user.creditCards.length > 0) {
					Actions.feed();
				} else {
					Actions.onBoarding();
				}
			} else {
				Actions.login();
			}
		}, stores.config.SplashTime);
	}

  /**
   * Component renderer.
   * 
   */
	render() {
    const { stores } = this.props;
    let { fadeAnim } = this.state;

		return (
      <View                 
        style={{
          flex: 1, 
          backgroundColor: '#f8e71c',
          justifyContent: 'center',
          alignItems: 'center'       
        }}>
        <Animated.Image 
          style={{ opacity: fadeAnim }}
          source={stores.config.logoImg} />
      </View >
		);
	}
}
