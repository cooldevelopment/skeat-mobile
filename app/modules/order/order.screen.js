import React, { Component } from 'react';
import WsConnection from '../../networking/ws';
// import PushNotification from 'react-native-push-notification';
import { Actions } from 'react-native-router-flux';

import { inject, observer } from 'mobx-react';
import { View, StyleSheet, Dimensions, Platform } from 'react-native';
import {
	Container,
	Content,
	H1,
	H2,
	Icon,
	Text,
	Button
} from 'native-base';

/**
 * Get app screen dimensions
 */
const { width } = Dimensions.get('window');

@inject("stores")
@observer
export default class OrderScreen extends Component {
	/**
	 * Component constructor.
	 * 
	 * @param {Object} props 
	 */
	constructor(props) {
		super(props);
		this.state = {
			accepted: false
		}
		this.ws = WsConnection.connect();
		this.handleReturn = this.handleReturn.bind(this);
	}

	/**
	 * Sending test notification
	 * 
	 */
	componentDidMount() {
		const { cart, auth } = this.props.stores;
		const companyChannel = `company:${cart.company_id}`;
		const orderChannel = `company:${cart.company_id}-order-${cart.order.id}`;
		const channelCom = this.ws.subscribe(companyChannel);
		const channel = this.ws.subscribe(orderChannel);

		/**
		 * Log on open events.
		 */
		this.ws.on('open', e => console.log(e))

		/**
		 * Update the order changes that happened outside the channel.
		 */
		this.setState({
			accepted: cart.order.status
		});
		
		if (cart.company_id) {
			/**
			 * EMit order to the compamny channel.
			 * 
			 */
			channelCom.emit('message', cart.order);

			/** 
			 * Get order channel subscription and update order status.
			 */
			channel.on('message', (order) => {
				console.log(order)
				if(typeof order === 'string') {
					this.setState({
						accepted: order
					});
				}

				if(typeof order === 'object') {
					cart.getLastOrder(auth.user.id);
				}

				/**
				 * This is a test notification.
				 */
				// PushNotification.localNotification({
				// 	title: "Pedido",
				// 	message: "Um item do seu pedido está pronto.",
				// 	playSound: false,
				// 	soundName: 'default'
				// });
			});
		}
	}

	componentDidUpdate() {
		const { cart, auth } = this.props.stores;
		cart.getLastOrder(auth.user.id);
	}

	/**
	 * Close all channels when the order is finished.
	 * 
	 */
	componentWillUnmount() {
		if (this.ws !== null) {
			this.ws.close();
		}
	}

	/**
	 * Handle the return to the feed page after order is finished.
	 * 
	 */
	handleReturn() {
		const { cart } = this.props.stores;
		this.ws.close();
		cart.resetCart()
		Actions.feed();
	}

	/**
	 * Navigation current title.
	 * 
	 */
	static navigationOptions = ({ navigation }) => ({
		title: 'Pedido',
	});

	/**
	 * Component renderer.
	 * 
	 */
	render() {
		const { cart } = this.props.stores;

		return (
			<Container style={{backgroundColor: "#ffffff"}}>
				{this.state.accepted === 'REFUSED' &&
					<Content
						contentContainerStyle={styles.container}>
						<View 
							style={[{
								paddingLeft: 15,
								paddingRight: 15
							}, styles.statusHeader]}>
							<H1 style={{ marginBottom: 15 }}>Pedido Cancelado!</H1>
							<H2 style={{ textAlign: 'center', alignSelf: 'center' }}>
								O restaurante não pode realizar seu pedido. :X
							</H2>
							<Button
								block
								dark
								bordered
								rounded
								style={{
									marginTop: 15,
									marginBottom: 10,
									backgroundColor: "#ffd740",
									borderColor: "#ffd740",
									paddingTop: Platform.OS === 'android' ? 15 : 0
								}}
								onPress={this.handleReturn}>
								<Text>VOLTAR</Text>
							</Button>
						</View>
					</Content>
				}
				{this.state.accepted !== 'ACCEPTED' && 
				this.state.accepted !== 'FINISHED' && 
				this.state.accepted !== 'REFUSED' &&
					<Content
						contentContainerStyle={styles.container}>
						<View 
							style={[{
								paddingLeft: 15,
								paddingRight: 15
							}, styles.statusHeader]}>
							<H1 style={{ marginBottom: 15 }}>Pedido Enviado!</H1>
							<H2 style={{ textAlign: 'center', alignSelf: 'center' }}>
								Aguardando aprovação do restaurante..
							</H2>
						</View>
					</Content>
				}
				{this.state.accepted === 'FINISHED' &&
					<Content
						contentContainerStyle={styles.container}>
						<View style={[{ 
								paddingLeft: 15, 
								paddingRight: 15 
							}, styles.statusHeader]}>
							<H1 style={{ marginBottom: 15 }}>Pedido Finalizado!</H1>
							<H2 style={{ textAlign: 'center', alignSelf: 'center' }}>
								Obrigado por usar o Skeat :] 
							</H2>
							<Button
								block
								dark
								bordered
								rounded
								style={{
									marginTop: 15,
									marginBottom: 10,
									backgroundColor: "#ffd740",
									borderColor: "#ffd740",
									paddingTop: Platform.OS === 'android' ? 15 : 0
								}}
								onPress={this.handleReturn}>
								<Text>VOLTAR</Text>
							</Button>
						</View>
					</Content>
				}
				{cart.order && this.state.accepted === 'ACCEPTED' &&
					<Content 
						contentContainerStyle={styles.container}>
							<View style={styles.statusHeader}>
								<H1>Sua senha é</H1>
								<H2 style={styles.orderCode}>{`SK-${cart.order.id}`}</H2>
							</View>
							<View style={{ flex: 4 }}>
							{cart.order.items && 
							cart.order.items.length && 
							cart.order.items.map((i) => {
									let productType;
									switch (i.product.category.type) {
										case 'bebidas':
											productType = 'local-bar';
											break;
										case 'diversos':
											productType = 'smoking-rooms';
											break;
										default:
											productType = 'local-pizza';
											break;
									}
									return (
										<View key={i.id} style={styles.statusWrapper}>
											<Icon
												name={productType}
												style={[
													styles.icon, 
													{ 
														backgroundColor: (i.status === 'PROCESSING') 
															? '#ffffff' 
															: '#ffd740'
													}
												]}/>
											<Text>
												{i.product.name}
												<Icon
													name={'done'}
													style={{
															marginLeft: 10,
															color: (i.status === 'PROCESSING')
																? '#fff'
																: '#000'
														}} />
											</Text>
										</View>
									)
								})}
								<View style={styles.statusWrapper}>
									<Icon
										name='done'
										style={styles.icon}/>
									<Text>Pedido Finlizado!</Text>
								</View>
							</View>
					</Content>
				}
			</Container>
		);
	}
}

/**
 * Component Styles.
 * 
 */
const styles = StyleSheet.create({
	container: {
		backgroundColor: '#ffffff',
		flex: 1,
		paddingTop: 15,
		alignItems: 'center',
	},
	statusHeader: {
		flex: 1,
		width: width,
		paddingBottom: 25,
		borderBottomColor: '#eeeeee',
		borderBottomWidth: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	orderCode: {
		color: '#ffd740',
		paddingTop: Platform.OS === 'android' ? 15 : 25,
		paddingBottom: Platform.OS === 'android' ? 15 : 10,
		paddingLeft: 25,
		paddingRight: 25,
		marginTop: 10,
		backgroundColor: '#000000',
		fontSize: 40
	},
	statusWrapper: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'flex-start',
		flexDirection: 'row',
		width: width,
		paddingLeft: 25
	},
	icon: {
		marginRight: 15,
		paddingTop: 5,
		paddingLeft: 3,
		fontSize: 50,
		color: '#000000',
		borderColor: '#000000',
		borderRadius: 10,
		borderWidth: 2,
		overflow: 'hidden'
	}
});

