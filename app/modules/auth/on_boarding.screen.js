import React, { Component } from 'react';
import {
	Container,
	Content,
	Button,
	Form,
	Item,
	Icon,
	Input,
	Text,
	H2,
	Toast
} from 'native-base';
import {
	View,
	StyleSheet,
	Platform
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text'
import { Actions } from 'react-native-router-flux';

import { inject, observer } from 'mobx-react';

@inject("stores")
@observer
export default class OnBoardingScreen extends Component {
	/**
	 * Component constructor.
	 * 
	 * @param {*} props 
	 * 
	 */
	constructor(props) {
		super(props);
		this.handleSaveUser = this.handleSaveUser.bind(this);
	}

	/**
	 * Veriofy if all required fields ahve been filled 
	 * and save in to the user.
	 * 
	 */
	handleSaveUser() {
		const { auth } = this.props.stores;

		if (auth.user.cpf.length > 0 && 
			auth.user.phone.length > 0 && 
			auth.user.birthday.length > 0) {
			auth.saveUser().then((res) => {
				if(!!res.data) {
					Actions.address();
				} else {
					Toast.show({
						text: "Ocorreu um erro ao salvar o usuário!",
						buttonText: "OK",
					});
				}
			});
		} else {
			Toast.show({
				text: "Preencha todos os campos marcados como obrigatórios (*).",
				buttonText: "OK",
			});
		}
	}

	/**
	 * Component renderer.
	 * 
	 */
	render() {
		const { auth } = this.props.stores;

		return (
			<Container style={{backgroundColor: "#ffffff"}}>
				<Content scrollEnabled={false} style={styles.container}>
					<View style={styles.header}>
						<H2>
							Complete seu Perfil
						</H2>
					</View>
					<View style={styles.loginForeground}>
						<Form>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='account-circle' />
								<Input
									value={auth.user.fullname}
									placeholder='* Qual seu nome completo?'
									required
									onChangeText={(fullname) => auth.handleChangeUser('fullname', fullname)}
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='date-range' />
								<TextInputMask
									style={{
										flex: 1,
										fontSize: 17,
										paddingRight: 5,
										height: 50,
										color: "#000000"
									}}
									// here we set the custom component and their props.
									customTextInputProps={{
										label:'Qual sua data de nascimento?'
									}}

									type={'datetime'}
									options={{
										format: 'DD/MM/YYYY'
									}}
									value={auth.user.birthday}
									onChangeText={(birthday) => auth.handleChangeUser('birthday', birthday)}
									placeholder="* Qual sua data de nascimento?"
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='phone' />
								<TextInputMask
									style={{
										flex: 1,
										fontSize: 17,
										paddingRight: 5,
										height: 50,
										color: "#000000"
									}}
									// here we set the custom component and their props.
									customTextInputProps={{
										label:'Qual seu telefone?'
									}}

									type={'cel-phone'}
									options={{
										format: '(99) 99999-9999'
									}}
									value={auth.user.phone}
									onChangeText={(phone) => auth.handleChangeUser('phone', phone)}
									placeholder="* Qual seu telefone?"
								/>
							</Item>
							<Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
								<Icon style={{color: "#ffd740"}} name='edit' />
								<TextInputMask
									style={{
										flex: 1,
										fontSize: 17,
										paddingRight: 5,
										height: 50,
										color: "#000000"
									}}
									customTextInputProps={{
										label:'Qual seu cpf?'
									}}
									type={'cpf'}
									options={{
										format: '999.999.999-99'
									}}
									value={auth.user.cpf}
									onChangeText={(cpf) => auth.handleChangeUser('cpf', cpf)}
									placeholder="* Qual seu cpf?" />
							</Item>
							<Button 
								block
								rounded
								style={{
									marginBottom: 10,
									paddingTop: Platform.OS === 'android' ? 15 : 0 
								}}
								onPress={this.handleSaveUser}>
								<Text>PRÓXIMO</Text>
							</Button>
						</Form>
					</View>
				</Content>
			</Container>
		);
	}
}

/**
 * Component styles.
 * 
 */
const styles = StyleSheet.create({
	container: {
		backgroundColor: '#ffffff',
	},
	header: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		paddingBottom: 20,
		paddingTop: 20,
		flex: 1,
	},
	radioText: {
		width: 100,
		flex: 1
	},
	loginForeground: {
		flex: 1,
		paddingTop: 20,
		paddingLeft: 10,
		paddingRight: 10,
		paddingBottom: 90,
		bottom: 0
	},
	list: {
		paddingBottom : 20
	}
});
