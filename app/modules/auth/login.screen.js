import React, { Component } from 'react';
import {
	Container,
	Content
} from 'native-base';
import {
	View,
	Image,
	StyleSheet
} from 'react-native';
import { inject } from 'mobx-react';
import Login from './../../components/login.component';

@inject("stores")
export default class LoginScreen extends Component {
  /**
   * Componenet constructor
   * 
   * @param {*} props 
   * 
   */
	constructor(props) {
		super(props);
	}

  /**
   * Component renderer.
   * 
   */
	render() {
		const { stores } = this.props;

		return (
			<Container style={{backgroundColor: "#ffffff"}}>
				<Content scrollEnabled={false} style={styles.container}>
					<View style={styles.logo}>
						<Image
							source={stores.config.logoImg}/>
					</View>
					<View style={styles.loginForeground}>
						<Login {...this.props} />
					</View>
				</Content>
			</Container>
		);
	}
}

/**
 * Componenet styles.s
 * 
 */
const styles = StyleSheet.create({
	container: {
		backgroundColor: '#ffffff',
		position: 'absolute',
		bottom: 0,
		left: 0,
		right: 0,
	},
	logo: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		paddingBottom: 20,
		flex: 1,
	},
	loginForeground: {
		flex: 1,
		paddingTop: 20,
		paddingLeft: 10,
		paddingRight: 10,
		paddingBottom: 90,
		bottom: 0
	}
});
