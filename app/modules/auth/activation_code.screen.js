/**
 * Created by matheusmorett on 03/06/18.
 */
import React, { Component } from 'react';
import {
    Container,
    Content,
    Button,
    Item,
    Input,
    Icon,
    Text,
    H2,
    Toast
} from 'native-base';
import {
    View,
    Image,
    StyleSheet,
    Dimensions
} from 'react-native';
import {inject} from 'mobx-react';
import { Actions } from 'react-native-router-flux';

@inject("stores")
export default class ActivationCoreScreen extends Component {

    constructor(props) {
        super(props);

        this.validateActivationCode = this.validateActivationCode.bind(this);
    }

    validateActivationCode() {
        const {auth} = this.props.stores;
        auth.validateActivationCode().then((res) => {
            if(!!res.data ) {
                if(!!res.data.fullname) {
                    Actions.feed();
                } else {
                    Actions.onBoarding();
                }
            } else {
                Toast.show({
                    text: "Código inválido!",
                    buttonText: "OK",
                });
            }
        });
    }

    render() {
        const {auth} = this.props.stores;
        return (
            <Container>
                <Content scrollEnabled={false} style={{
                    backgroundColor: "#ffffff",
                    padding: 10,
                    paddingTop: 250
                }}>

                    <View style={styles.logo}>
                        <H2> Um código de ativação foi enviado ao seu celular </H2>
                        <Text> Digite o código abaixo </Text>
                    </View>
                    <Item style={{marginBottom: 10, borderColor: "#ffd740"}} rounded>
                        <Icon style={{color: "#ffd740"}} name='lock' />
                        <Input
                            placeholder='Código de ativação'
                            onChangeText={(code) => auth.handleChange('activationCode', code)}
                        />
                    </Item>
                    <Button block
                            rounded
                            style={{marginBottom: 10}}
                            onPress={this.validateActivationCode}
                    >
                        <Text>ATIVAR</Text>
                    </Button>
                </Content>
            </Container>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff"
    },
    logo: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        paddingBottom: 20,
        flex: 1,
    },
    loginForeground: {
        flex: 1,
        paddingTop: 20,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 90,
        bottom: 0
    }
});
