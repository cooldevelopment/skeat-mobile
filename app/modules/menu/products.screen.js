import React, { Component } from 'react';
import {inject, observer} from 'mobx-react';
import {
	StyleSheet,
	Dimensions,
	View,
	TouchableOpacity,
	LayoutAnimation,
	Platform
} from 'react-native';
import Cart from './../../components/cart.component';
import {
	Container,
	Content,
	List,
	ListItem,
	Text,
	Left,
	Right,
	Icon,
	Body,
	Button
} from 'native-base';

/**
 * Collapse animation.
 * 
 */
const CustomLayoutAnimation = {
	duration: 200,
	create: {
		type: LayoutAnimation.Types.linear,
		property: LayoutAnimation.Properties.opacity,
	},
	update: {
		type: LayoutAnimation.Types.curveEaseInEaseOut,
	},
};

@inject("stores")
@observer
export default class ProductsScreen extends Component {
	/**
	 * Component constructor.
	 * 
	 */
	constructor(props) {
		super(props);
		this.state = {
			noResults: 'Nenhum produto encontrado.'
		}
	}

	/**
	 * On press expand product description.
	 * 
	 */
	onPress(index) {
		// Uncomment to animate the next state change.
		LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);

		// Or use a Custom Layout Animation
		// LayoutAnimation.configureNext(CustomLayoutAnimation);

		if (this.state.index === index) {
			this.setState({index: undefined});
		} else {
			this.setState({index: index});
		}
	}

	/**
	 * Get products when componint mount.
	 * 
	 */
	componentDidMount() {
		const {stores, category_id} = this.props;
		stores.menu.getProductsByCategory({category_id});
	}

	/**
	 * Nvigation title params.
	 * 
	 */
	static navigationOptions = ({navigation}) => ({
		title: 'Produtos',
	});

	/**
	 * Component renderer.
	 * 
	 */
	render() {
		const {stores} = this.props;
		const {noResults} = this.state;

		return (
			<Container>
				<Content scrollEnabled={false} style={styles.container}>
					<View style={styles.view}>
						{stores.menu.products.length === 0 && <Text style={styles.title}>{noResults}</Text>}
						<List>
							{stores.menu.products.length > 0 && stores.menu.products.map((p, index) => {
								return (
									<ListItem key={p.id} style={{ marginRight: 20 }}>
												<Left style={{ flex: 1 }}>
													<Body>
														<TouchableOpacity onPress={() => this.onPress(index)}>
															<Text>{p.name}</Text>
															{(this.state.index === index &&
																<Text style={{
																	fontSize: 12
																}}>{p.description}</Text>
															)}
															<Text style={{
																fontSize: 12,
															}}>R$ {p.price.toFixed(2)}</Text>
														</TouchableOpacity>
													</Body>
												</Left>
											<Right style={{ flex: 1 }}>
												<View style={styles.actions}>
													<Button
														rounded
														light
														disabled={
															stores.cart.getQuantitySelected(p.id) > 0 
																? false 
																: true
															}
														onPress={() => stores.cart.removeProduct(p.id)}
														style={styles.actionbutton}>
														<Icon name="remove" style={styles.icon}/>
													</Button>
												<Text 
													style={{
														marginRight: 10,
														marginLeft: 10
													}}>
														{stores.cart.getQuantitySelected(p.id)}
													</Text>
													<Button
														rounded
														light
														onPress={() => stores.cart.addProduct(p.id)}
														style={styles.actionbutton}>
														<Icon name="add" style={styles.icon}/>
													</Button>
												</View>
											</Right>
										</ListItem>
								);
							})}
						</List>
						<Cart />
					</View>
				</Content>
			</Container>
		);
	}
}

/**
 * Get app screen dimensions.
 * 
 */
const {height} = Dimensions.get('window');

/**
 * Component styles.
 * 
 */
const styles = StyleSheet.create({
	container: {
		backgroundColor: "#fff",
		padding: 1,
		flex: 1,
		height: height,
	},
	view: {
		flex: 1,
		height: height,
	},
	actions: {
		flex: 1,
		flexDirection: 'row',
		height: 100,
		justifyContent: 'center',
		alignItems: 'center'
	},
	actionbutton: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 40,
		height: 40,
		paddingLeft: Platform.OS === 'android' ? 10 : 0,
		paddingTop: Platform.OS === 'android' ? 15 : 5
	},
	title: {
		color: '#000',
		fontSize: 20,
		textAlign: 'left',
		marginVertical: 5,
		paddingTop: 20,
		paddingLeft: 20
	},
	icon: {
		fontSize: 20,
		width: Platform.OS === 'android' ? 30 : 20,
		height: Platform.OS === 'android' ? 30 : 20,
		color: '#000',
	}
});

