import React, { Component } from 'react';
import {inject, observer} from 'mobx-react';
import {Actions} from 'react-native-router-flux';
import {
	StyleSheet,
	Dimensions,
	View
} from 'react-native';
import {
	Container,
	Body,
	CheckBox,
	Content,
	List,
	ListItem,
	Text,
	Left,
	Right,
	Button
} from 'native-base';

@inject("stores")
@observer
export default class AdditionalsScreen extends Component {
  /**
   * Component constructor.
   * 
   * @param {Object} props 
   * 
   */
	constructor(props) {
		super(props);
		this.state = {
			product_id: null
		}
	}

	/**
	 * Get the selected product is.
	 * 
	 */
	componentDidMount() {
		const { product_id } = this.props;
		this.setState({
			product_id: product_id
		})
	}

  /**
   * Navigation params
   * 
   */
	static navigationOptions = ({navigation}) => ({
		title: 'Editar',
	});

  /**
   * Component renderer.
   * 
   */
	render() {
		const { cart, menu } = this.props.stores;
		const { product_id } = this.state;
		const products = menu.getProduct(product_id);

		return (
			<Container>
				<Content 
					scrollEnabled={true} 
					style={styles.container}>
					<View style={styles.view}>
					<List>
						<ListItem itemDivider>
							<Text>Ingredientes</Text>
						</ListItem>
						{products !== undefined &&
							products.ingredients.map((i, index) => {
							return (
								<ListItem key={index} style={{ marginRight: 20 }}>
									<Left style={{
										marginLeft: (i.customizable > 0) ? 0 : 30
									}}>
										{i.customizable > 0 &&
											<CheckBox 
												onPress={() => cart.changeIngredient(i)}
												checked={(
														(i.customizable === 0 || 
															cart.selectedItem
															.ingredients
															.findIndex((ig) => ig.id === i.id) !== -1)
														? false
														: true
												)}
												style={styles.select}/>}
										<Body>
										<Text>{i.name}</Text>
										</Body>
									</Left>
								</ListItem>
							);
						})}
						</List>
						<List>
						<ListItem itemDivider>
							<Text>Adicionais</Text>
						</ListItem>
						{products !== undefined &&
								products.additionals.map((a, index) => {
							return (
								<ListItem key={index} style={{ marginRight: 20 }}>
									<Left>
										<CheckBox 
											onPress={() => cart.changeAdditional(a)}
											checked={
												(cart.selectedItem
													.additionals
													.findIndex((ad) => ad.id === a.id) !== -1)
													 ? true
													 : false
											}
											style={styles.select} />
										<Body>
										<Text>{a.name}</Text>
										</Body>
									</Left>
									<Right>
										<Body>
											<Text>R$ {parseFloat(a.price).toFixed(2)}</Text>
										</Body>
									</Right>
								</ListItem>
							);
						})}
					</List>
					</View>
					<View 
						style={{
							flex: 1,
							paddingLeft: 10,
							paddingRight: 10,
							marginBottom: 30
						}}>
						<Button 
							rounded
							block 
							style={{
								backgroundColor: "#fff159"
							}} 
							onPress={() => {
								cart.saveChanges();
								Actions.pop();
							}}>
							<Text style={{color: "#000"}}>Salvar</Text>
						</Button>
					</View>
				</Content>
			</Container>
		);
	}
}

/**
 * Component Styles.
 * 
 */
const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
	container: {
		backgroundColor: "#fff",
		padding: 1,
		flex: 1,
		height: height,
	},
	view: {
		flex: 1,
		marginBottom: 30
		// height: height
	},
	cartHeader: {
		height: 50,
		backgroundColor: '#fffe35',
		paddingLeft: 8,
		paddingRight: 20,
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-start'
	},
	cartTitle: {
		color: "#000"
	},
	cartContainer: {
		flexDirection: 'column',
		justifyContent: 'flex-end',
		alignItems: 'flex-end',
	},
	select: {
		marginRight: 10
	}
});
