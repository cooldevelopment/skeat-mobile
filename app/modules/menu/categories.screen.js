import React, { Component } from 'react';
import {inject, observer} from 'mobx-react';
import { StyleSheet, Dimensions } from 'react-native';
import Cart from './../../components/cart.component';
import { Container, Content, View, Text } from 'native-base';
import CarouselComponent from './../../components/carousel.component';

@inject("stores")
@observer
export default class CategoriesScreen extends Component {
  /**
   * Component constructor.
   *  
   */
	constructor(props) {
		super(props);
		this.state = {
			noResults: 'Nenhuma categoria encontrada.'
		}
	}

  /**
   * Fetch categories for this current company.
   * 
   */
	componentDidMount() {
		const { stores, company_id } = this.props;
		stores.menu.getCategoriesByCompany({ company_id });
	}

  /**
	 * Navigation current title.
	 * 
	 */
	static navigationOptions = ({ navigation }) => ({
		title: 'Categorias'
	});

  /**
   * Component renderer.
   * 
   */
	render() {
		const { categories, showMessage } = this.props.stores.menu;
		const { noResults } = this.state;

		return (
			<Container>
				<Content scrollEnabled={false} style={styles.container}>
					<View style={styles.view}>
						<View>
							{showMessage && <Text style={styles.title}>{noResults}</Text>}
							{categories.foods &&
								<View style={styles.item}>
									<CarouselComponent 
										key={1} 
										type="Comidas" 
										data={categories.foods}/>
								</View>}
							{categories.drinks &&
								<View style={styles.item}>
									<CarouselComponent 
										key={2} type="Bebidas" 
										data={categories.drinks}/>
								</View>}
							{categories.others &&
								<View style={styles.item}>
									<CarouselComponent 
										key={3} 
										type="Diversos" 
										data={categories.others}/>
								</View>}
						</View>
						<Cart />
					</View>
				</Content>
			</Container>
		);
	}
}

/**
 * Component styles.
 * 
 */
const {width, height} = Dimensions.get('window');
const heightScale = Math.round(height / 4.3);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff'
	},
	view: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: 'center',
		width: width,
		height: height,
		paddingTop: 10
	},
	item: {
		flexDirection: 'row',
		width: Dimensions.get('window').width,
		height: heightScale + 5,
		marginTop: 10,
		zIndex: 0
	},
	title: {
		color: '#000',
		fontSize: 20,
		padding: 15,
		textAlign: 'left',
		marginVertical: 5
	}
});

