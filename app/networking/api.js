/**
 * Created by matheusmorett on 03/06/18.
 */
import { AsyncStorage } from 'react-native';
import { API_ROOT } from './api.config';

class Api {

    async call(url, method, params) {
        const auth = await this.getUserAuthorization();
        return fetch(API_ROOT + url, {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': auth
            },
            body: JSON.stringify(params),
        }).then((response) => {
            return response.json();
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error;
        });
    }

    async getUserAuthorization() {
        const token = await AsyncStorage.getItem('token');

        if(token) {
            return `Bearer ${token}`;
        }

        return null;
    }
}

export default new Api();