import Ws from '@adonisjs/websocket-client';

/**
 * Socket API endpoint.
 * 
 */
const WS_URL = "wss://api.skeat.com.br";

const WsConnection = Ws(WS_URL, {
  reconnectionAttempts: 1
});

export default WsConnection;
