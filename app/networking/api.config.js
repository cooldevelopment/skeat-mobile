/**
 * Set API config to get enviroment URL
 * 
 * 
 * API version: 1.0
 * 
 */
const apiVersion = "v1";

/**
 * Local API enviroment URL
 */
const apiHost = "http://localhost:3333";

/**
 * Local API enviroment URL
 */
// const apiHost = "https://api.skeat.com.br";

export const API_ROOT = `${apiHost}/api/${apiVersion}`;