/**
 * Created by matheusmorett on 16/07/18.
 */
export const URL_VIA_CEP = "https://viacep.com.br/ws/";
export const TYPE_JSON = "/json";

class Helpers {

    getAddressByViaCep(cep) {
        console.log(cep);
        const finalUrl = URL_VIA_CEP + cep + TYPE_JSON;
        return fetch(finalUrl, {
            method: 'GET',
            headers: {
                'accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then((response) => {
            return response.json();
        }).then((response) => {
            return response;
        }).catch((error) => {
            return error;
        });
    }

    makeShortUrl(url) {

        return new Promise((resolve, reject) => {
            const gapi = window.gapi;

            gapi.load('client', () => {
                gapi.client.setApiKey('AIzaSyCLnkOPorFv5ZF6dPAlqv_xa5o4jIFiX7Q');
                gapi.client.load('urlshortener', 'v1', () => {

                    const request = gapi.client.urlshortener.url.insert({
                        'resource': {
                            'longUrl': url
                        }
                    });

                    request.execute((response) => {

                        if (response.id != null) {
                            resolve(response.id);
                        }

                    });
                });
            });
        });

    };

    getAllUrlParams(url) {

        // get query string from url (optional) or window
        let queryString = url ? url.split('?')[1] : window.location.search.slice(1);

        // we'll store the parameters here
        let obj = {};

        // if query string exists
        if (queryString) {

            // stuff after # is not part of query string, so get rid of it
            queryString = queryString.split('#')[0];

            // split our query string into its component parts
            let arr = queryString.split('&');

            for (let i = 0; i < arr.length; i++) {
                // separate the keys and the values
                let a = arr[i].split('=');

                // in case params look like: list[]=thing1&list[]=thing2
                let paramNum = undefined;
                let paramName = a[0].replace(/\[\d*\]/, function (v) {
                    paramNum = v.slice(1, -1);
                    return '';
                });

                // set parameter value (use 'true' if empty)
                let paramValue = typeof(a[1]) === 'undefined' ? true : a[1];

                // (optional) keep case consistent
                paramName = paramName.toLowerCase();
                paramValue = paramValue.replace(/%20/g, " ");

                // if parameter name already exists
                if (obj[paramName]) {
                    // convert value to array (if still string)
                    if (typeof obj[paramName] === 'string') {
                        obj[paramName] = [obj[paramName]];
                    }
                    // if no array index number specified...
                    if (typeof paramNum === 'undefined') {
                        // put the value on the end of the array
                        obj[paramName].push(paramValue);
                    }
                    // if array index number specified...
                    else {
                        // put the value at that index number
                        obj[paramName][paramNum] = paramValue;
                    }
                }
                // if param name doesn't exist yet, set it
                else {
                    obj[paramName] = paramValue;
                }
            }
        }

        return obj;
    }

    getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[[\]]/g, "\\$&");
        const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    hasClass(el, className) {
        if (el.classList)
            return el.classList.contains(className)
        else
            return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
    }

    addClass(el, className) {
        if (el.classList)
            el.classList.add(className)
        else if (!this.hasClass(el, className)) el.className += " " + className
    }

    removeClass(el, className) {
        if (el.classList)
            el.classList.remove(className)
        else if (this.hasClass(el, className)) {
            const reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
            el.className = el.className.replace(reg, ' ')
        }
    }

    uniqueId() {
        return '_' + Math.random().toString(36).substr(2, 9);
    }

}

export default new Helpers();