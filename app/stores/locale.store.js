import {observable, action} from 'mobx';
import Api from './../networking/api';

export default class LocaleStore {

	@observable locales = [];
	@observable locale;
	@observable loading = false;

	@action
	getLocaleByLatLng(params) {
    this.loading = true;
		return Api.call('/locales/get-by-lat-lng', 'post', params)
    .then((res) => {
				this.loading = false;
				this.locales = res.data[0].map((l) => {
					l.label = l.name;
					l.key = l.id;
					return l;
				});
				this.locale = res.data[0][0];
				return res;
			}).catch((err) => {
				return err;
			});
	}

	@action
	setLocale(id) {
		this.locale = this.locales.find((l) => l.id === id);
	}
}