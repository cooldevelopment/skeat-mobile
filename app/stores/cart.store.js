import { observable, action, computed } from 'mobx';
import { AsyncStorage } from 'react-native';
import Api from './../networking/api';
import MenuStore from './menu.store';

class Ingredient {
	@observable customizable;
	@observable selected;

	constructor(props) {
		this.id = props.id || '';
		this.name = props.name || '';
		this.customizable = props.customizable;
	}
}

class Additional {
	@observable selected;

	constructor(props) {
		this.id = props.id || '';
		this.name = props.name || '';
		this.price = props.price || '';
	}
}
export default class CartStore {

	@observable company_id = null;
	@observable discount = 0;
	@observable items = [];
	@observable order = [];
	@observable selectedItem = {
		ingredients: [],
		additionals: []
	};
	
	getQuantitySelected(id) {
		const value = this.items
			.reduce(( prevVal, i ) => {
				if(i.id === id) {
					return prevVal + 1;
				}
				return prevVal;
			}, 0 );
		return value;
	}

	/**
	 * Find incomming item in the items array and 
	 * update with the new one
	 * 
	 * @param {Object} item
	 * 
	 */
	@action
	updateOrderItemStatus(item) {
		/**
		 * Find index in the array
		 */
		const itemIndex = this.order
			.items
			.findIndex((i) => {
				return i.id === item.id;
			});

		/**
		 * Update with new item.
		 */
		if(itemIndex !== -1) {
			this.order.items[itemIndex] = item;
		}
	}

	@action
	deleteItem(indexValue) {
		this.items.splice(indexValue, 1);
	}

	@action
	addProduct(id) {
		const product = MenuStore.getProduct(id);
		
		this.items.push({
			id: id,
			name: product.name,
			value: product.value,
			price: product.price,
			company_id: product.company_id,
			ingredients: [],
			additionals: []
		});
		this.company_id = product.company_id;
	}

	updateOrder() {
		this.company_id = this.items
			.map(c => c.company_id)
			.filter((item, pos, s) => {
				return s.indexOf(item) == pos;
			});
	}
		
	@action
	removeProduct(id) {
		const index = this.items
			.findIndex((i) => i.id === id);
		this.items.splice(index, 1);
	}

	/**
	| 
	| Here are the handlers to the customization of an item.
	|
	| customizeItem
	| changeIngredient
	| changeAdditional
	| saveChanges
	|
	*/
	
	/**
	 * Get the current selected item to customize it`s ingredients 
	 * and additionals.
	 * 
	 * @param {Object} item 
	 * @param {Number} indexValue 
	 * 
	 */
	@action
	customizeItem(item, indexValue) {
		this.selectedItem = Object.assign({}, item);
		this.selectedItem.indexValue = indexValue;
	}

	/**
	 * Customize the ingredients that are available to.
	 * 
	 * @param {Number} id 
	 * 
	 */
	@action
	changeIngredient(ingredient) {
		/**
		 * If the ingredient is customizable allow the user
		 * to remove from the item.
		 */
		if (ingredient.customizable === 1) { 
			const findIndex = this.selectedItem
				.ingredients
				.findIndex((i) => i.id === ingredient.id);

			/**
			 * Replace the ingredient in the current item.
			 */
			if(findIndex !== -1) { 
				this.selectedItem
					.ingredients
					.splice(findIndex, 1);
			} else {
				this.selectedItem
					.ingredients
					.splice(findIndex, 0, ingredient)
			}
		}
	}

	/**
	 * Add or remove additionals to the current item.
	 * 
	 * @param {Number} id 
	 * 
	 */
	@action
	changeAdditional(additional) {
		/**
		 * Find the index of the additional.
		 */
		const findIndex = this.selectedItem
			.additionals
			.findIndex((i) => i.id === additional.id);

		/**
		 * Replace the additional in the current item.
		 */
		if(findIndex !== -1) { 
			this.selectedItem
				.additionals
				.splice(findIndex, 1)
		} else {
			this.selectedItem
				.additionals
				.splice(findIndex, 0, additional)
		}
	}

	/**
	 * Saves the changes made to the additionals and the 
	 * optionals ingredients in the cart items.
	 * 
	 */
	@action
	saveChanges() {
		const findIndex = this.items
			.findIndex((i, index) => {
				i.id === this.selectedItem.id && 
				index === this.selectedItem.indexValue}
			);
		this.items.splice(findIndex, 1, this.selectedItem);
		this.selectedItem = {
			ingredients: [],
			additionals: []
		};
	}

	/**
	 * Set the new discount value when needed.
	 * 
	 */
	@action
	updateDiscount(discount = 0) {
		this.discount = discount;
	}
	
	/**
	 * Loop trough all the products and it`s addtionals 
	 * added to the cart ti get the total price.
	 * 
	 */
	getAmount() {
		/**
		 * Get the total amlount from the items.
		 */
		const amount = this.items
			.reduce(( itemPrice, i ) => {
				itemPrice += i.price;
				itemPrice += i.additionals
					.reduce(( additionalPrice, a ) => {
						if(a.selected === true) {
							additionalPrice += a.price;
						}
						return additionalPrice;
					}, 0 );
				return itemPrice;
			}, 0 );

		/**
		 * If there is a dicount coupon sub the value 
		 * from the total cart price and return the updated price.
		 */
		const discountAmount = (this.discount * amount) / 100;
		return amount - discountAmount;
	}

	@action
	async getLastOrder(userId) {
		if (userId) {
			return Api.call('/orders/get-by-user', 'post', { 
				user_id: userId
			})
				.then((res) => {
					return this.order = res.order[0];
				}).catch((err) => {
					return err;
				});
		}
	}
		
	/**
	 * Send order to API and return the received data.
	 * Else return catch error.
	 * 
	 * @param {Int} user 
	 */
	saveOrder(user) {
		/**
		 * create order object with the current company id mapped 
		 * from the items array and return all products id
		 * as well as the user id. 
		 */
		const order = {
			company_id: this.company_id,
			items: this.items.map((p) => {
				if (p !== undefined && p !== null) {
					return {
						id: p.id,
						additionals: p.additionals.map(a => a.name).join(', '),
						ingredients: p.ingredients.map(i => i.name).join(', ')
					}
				}
			}),
			user_id: user
		};
			
		/**
		 * Call API with the order data and get the created record 
		 * from the response.
		 */
		return Api.call('/orders', 'post', order)
			.then((res) => {
				this.order = res.data;
				return res;
			}).catch((err) => {
				return err;
			});
	}

	/**
	 * Reset items in the cart.
	 * 
	 */
	@action
	resetCart() {
		this.items = [];
		this.order = [];
		this.company_id = null;
		this.selectedItem = {};
		AsyncStorage.removeItem('lastOrder');
	}

	/**
	 * Set the current company to the cart.
	 * 
	 * @param {Number} id 
	 * 
	 */
	@action
	cartCompany(id) {
		this.company_id = id;
	}
}