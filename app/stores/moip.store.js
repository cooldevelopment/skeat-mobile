/**
 * Created by matheusmorett on 16/07/18.
 */
import {observable, action} from 'mobx';
import { AsyncStorage } from 'react-native';
import moment from 'moment';
import Helpers from './../utils/Helpers';
import Api from './../networking/api';

class CreditCard {
    @observable fullname;
    @observable birthday;
    @observable cpf;
    @observable expirationYear;
    @observable expirationMonth;
    @observable number;
    @observable cvc;
}

export default class MoipStore {

    @observable creditCard = {
        fullname: null,
        birthday: null,
        cpf: 'M',
        expirationMonth: '',
        expirationYear: '',
        number: '',
        cvc: '',
    };
    @observable change = false;

    @action
    handleChangeCreditCard(name, value) {
        this.creditCard[name] = value;
        this.change = !this.change;
    }

}

