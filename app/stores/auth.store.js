import {observable, action} from 'mobx';
import { AsyncStorage } from 'react-native';
import moment from 'moment';
import Helpers from './../utils/Helpers';
import Api from './../networking/api';

class CreditCard {
	@observable fullname;
	@observable birthday;
	@observable cpf;
	@observable expirationYear;
	@observable expirationMonth;
	@observable number;
	@observable cvc;
	@observable brand;
	@observable credit_card_id;
	@observable first6;
	@observable last4;

	constructor(params = {}) {
		this.fullname = params.fullname || '';
		this.birthday = params.birthday || '';
		this.cpf = params.cpf || '';
		this.expirationYear = params.expirationYear || '';
		this.expirationMonth = params.expirationMonth || '';
		this.number = params.number || '';
		this.cvc = params.cvc || '';
		this.brand = params.brand || '';
		this.credit_card_id  = params.credit_card_id || '';
		this.first6 = params.first6 || '';
		this.last4 = params.last4 || '';
	}
}

class User {
	@observable id;
	@observable fullname;
	@observable birthday;
	@observable cpf;
	@observable phone;
	@observable sex;
	@observable cep;
	@observable city;
	@observable street;
	@observable complement;
	@observable district;
	@observable street_number;
	@observable state;
	@observable orders;
	@observable creditCards;

	constructor(params = {}) {
		this.id = params.id || '';
		this.fullname = params.fullname || '';
		this.cpf = params.cpf || '';
		this.phone = params.phone || '';
		this.birthday = params.birthday || '';
		this.sex = params.sex || 'M';
		this.cep = params.cep || '';
		this.city = params.city || '';
		this.street = params.street || '';
		this.complement = params.complement || '';
		this.district = params.district || '';
		this.street_number = params.street_number || '';
		this.state = params.state || '';
		this.orders = params.orders || [];
		this.creditCards = (params.creditCards) 
			? params.creditCards.map((cc) => { return new CreditCard(cc) }) 
			: [];
	}
}

export default class AuthStore {
	@observable user;
	@observable creditCard = new CreditCard();
	@observable change = false;
	@observable phone = "";
	@observable activationCode = "";
	@observable isLogged = false;

	constructor() {
		this.getUser();
	}

	async getUser() {
		const user = await AsyncStorage.getItem('user');
		if (user !== null) {
			this.user = new User(JSON.parse(user));
			this.creditCard = new CreditCard();
			this.isLogged = true;
		} else {
			this.user = new User();
			this.creditCard = new CreditCard();
		}
	}

	@action
	sendActivationCode() {
		return Api.call('/activation-code/send', 'post', { phone: this.phone });
	}

	@action
	validateActivationCode() {
		const params = {
			activation_code: this.activationCode,
			phone: this.phone
		};
		return Api.call('/activation-code/authenticate', 'post', params).then((res) => {
			if(!!res.data) {
				this.user = res.data;
				AsyncStorage.setItem('user', JSON.stringify(res.data));
				AsyncStorage.setItem('token', res.token.token);
				this.isLogged = true
			}
			return res;
		});
	}

	@action
	socialLogin(params) {
		return Api.call('/facebook/callback', 'post', params).then((res) => {
			if (!!res.data) {
				this.user = new User(res.data);
				AsyncStorage.setItem('user', JSON.stringify(res.data));
				AsyncStorage.setItem('token', res.token.token);
				this.isLogged = true
			}
			return res;
		});
	}

	@action
	handleChange(name, value) {
		this[name] = value;
	}

	@action
	handleChangeUser(name, value) {
		if (['cpf', 'phone'].includes(name)) {
			value = value.replace('(', '');
			value = value.replace(')', '');
			value = value.replace('.', '');
			value = value.replace('-', '');
		}

		this.user[name] = value;

		this.change = !this.change;

		if(name === 'cep' && value.length === 9) {
			Helpers.getAddressByViaCep(value).then((res) => {
				this.user.city = res.localidade;
				this.user.state = res.uf;
				this.user.street = res.logradouro;
				this.user.district = res.bairro;
				this.change = !this.change;
			});
		}
		this.change = !this.change;
	}

	@action
	handleChangeUserCreditCard(name, value) {
		this.creditCard[name] = value;
		this.change = !this.change;
	}

	@action
	async saveUser() {
		const params = {
			fullname: this.user.fullname,
			sex: this.user.sex,
			birthday: moment(this.user.birthday).format("Y-m-d").split("T")[0]
		};
		return await Api.call(`/users/${this.user.id}`, 'put', params).then((res) => {
			return res;
		});
	}
	
	@action
	async saveUserAsCustomer() {
		const params = {
			fullname: this.user.fullname,
			email: this.user.email,
			cpf: this.user.cpf,
			phone: this.user.phone,
			city: this.user.city,
			district: this.user.district,
			state: this.user.state,
			street: this.user.street,
			street_number: this.user.street_number,
			cep: this.user.cep,
			sex: this.user.sex,
			birthday: moment(this.user.birthday).format("YYYY-MM-DD")
		};
		return await Api.call('/payment/customers', 'post', params)
			.then((res) => {
				return res;
			});
	}
	
	@action
	async saveUserCreditCard() {
		const params = {
			fullname: this.user.fullname,
			expirationMonth: this.creditCard.expirationMonth,
			expirationYear: this.creditCard.expirationYear,
			number: this.creditCard.number,
			cvc: this.creditCard.cvc,
		};
		return await Api.call('/payment/card', 'post', params)
			.then((res) => {
				if (res.updatedUser) {
					this.user = new User(res.updatedUser);
					AsyncStorage.setItem('user', JSON.stringify(res.updatedUser));
				}
				return res;
			})
			.catch(err => console.log(err));
	}

}
