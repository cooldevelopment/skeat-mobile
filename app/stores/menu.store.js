import {observable, action} from 'mobx';
import Api from './../networking/api';

class MenuStore {

	@observable company_id;
	@observable showMessage = false;
	@observable categories = [];
	@observable products = [];

	@action
	getCategoriesByCompany(params) {
		this.loading = true;
		this.showMessage = false;

		return Api.call('/categories/get-by-company', 'post', params)
			.then((res) => {
				this.loading = false;
				this.categories = res;
				if (res.foods.length === 0 && 
					res.drinks.length === 0 && 
					res.others.length === 0) {
					this.showMessage = true;
				}
				return res;
			}).catch((err) => {
				return err;
			});
	}

	@action
	getProductsByCategory(params) {
		this.loading = true;
		
		return Api.call('/products/get-by-category', 'post', params)
			.then((res) => {
				this.loading = false;
				this.products = res.products;
				return res;
			}).catch((err) => {
				return err;
			});
	}

	@action
	getSelectedProducts(id) {
		const product = this.products.find((p) => p.id === id);
		return product.quantity;
	}

	@action
	getProduct(id) {
		return this.products.find((p) => p.id === id);
	}

	@action
	getItemsQuantityByProducts(id) {
		// Empty function for the moment
	}

}

export default new MenuStore();