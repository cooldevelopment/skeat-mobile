import {observable, action} from 'mobx';
import Api from './../networking/api';

export default class CompanyStore {

	@observable companies = [];
	@observable noResult = false;
	@observable loading = false;

	@action
	getCompaniesByLocale(id) {
    this.loading = true;
		const params = { locale_id: id };
    
    return Api.call('/companies/get-by-locale', 'post', params)
      .then((res) => {
        this.loading = false;
        this.companies = res.data;
        if(this.companies.length === 0) {
          this.noResult = true;
        }
        return res;
      }).catch((err) => {
        return err;
      });
	}
}
