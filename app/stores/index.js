/**
 * Created by matheusmorett on 31/05/18.
 */
import ConfigStore from './config.store';
import AuthStore from './auth.store';
import LocaleStore from './locale.store';
import CompanyStore from './company.store';
import MenuStore from './menu.store';
import CartStore from './cart.store';
import MoipStore from './moip.store';

const config = new ConfigStore();
const auth = new AuthStore();
const locale = new LocaleStore();
const company = new CompanyStore();
const menu = MenuStore;
const cart = new CartStore();
const moip = new MoipStore();


export default {config, auth, locale, company, menu, cart, moip};
