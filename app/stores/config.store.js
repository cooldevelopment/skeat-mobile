export default class ConfigStore {
    constructor() {
        this.splashTime = 1500;
        this.splashImg = require('../../images/splash.jpg');
        this.loginBG = require('../../images/login.jpg');
        this.logoImg = require('../../images/skeat.png');
    }

    get SplashImg() {
        return this.splashImg;
    }

    get SplashTime() {
        return this.splashTime;
    }

    get LoginBG() {
        return this.loginBG;
    }

    get LogoImg() {
        return this.logoImg;
    }
}